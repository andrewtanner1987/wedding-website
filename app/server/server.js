require('dotenv').config();

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const sessions = require('express-session');

app.use(sessions({
    resave: false,
    saveUninitialized: false,
    secret: process.env.SECRET
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let session;
app.get('/', (req, res) => {
    session = req.session;
    session.password;

    if (!session.password) {
        res.redirect('/login');
    } else {
        res.sendFile('/index.html', {
            root: '../public/'
        });
    
        app.use('/', express.static('../public/'));
    }
});

app.post('/login', (req, res) => {
    session = req.session;
    session.password = req.body.password;

    if (session.password == process.env.PASSWORD) {
        res.redirect('/');
    } else {
        res.redirect('/login?form-error=password');
    }
});

app.get('/login', (req, res) => {
    res.sendFile('/login.html', {
        root: '../public/'
    });

    app.use('/', express.static('../public/'));
});

app.listen(process.env.EXPRESS_PORT, () => {
    console.log(`Server listening on ${process.env.EXPRESS_PORT}...`);
});