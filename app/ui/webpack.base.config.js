const HtmlWebpackPlugin = require('html-webpack-plugin');
const Path = require('path');

module.exports = {
    entry: {
        app: "./index.js",
        auth: "./auth.js"
    },
    devtool: "cheap-eval-source-map",
    module: {
        rules: [
            {
                test: /\.(woff2?|png|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]" 
                        }
                    }
                ]
            },
            {
                test: /\.s?css$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "postcss-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: [
                    "babel-loader?cacheDirectory"
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "src/index.html",
            excludeChunks: ["auth"]
        }),
        new HtmlWebpackPlugin({
            filename: "login.html",
            template: "src/login.html",
            chunks: ["auth"]
        })
    ],
    output: {
        path: Path.resolve(__dirname, '../public')
    }
}
