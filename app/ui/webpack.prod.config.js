const WebpackMerge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const Base = require('./webpack.base.config');

const minifyHtml = {
    collapseWhitespace: true,
    removeComments: true,
    removeRedundantAttributes: true,
    removeScriptTypeAttributes: true,
    removeStyleLinkTypeAttributes: true,
    useShortDoctype: true
};

module.exports = WebpackMerge.smart(
    Base,
    {
        mode: "production",
        devtool: "source-map",
        module: {
            rules: [
                {
                    test: /\.(woff2?|png|svg)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "[name].[hash].[ext]" 
                            }
                        }
                    ]
                },
                {
                    test: /\.s?css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        "postcss-loader",
                        "sass-loader"
                    ]
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(["public"], {
                root: __dirname.replace("ui", "")
            }),
            new MiniCssExtractPlugin({
                filename: "[name].[hash].css",
                chunkFileName: "[id].[hash].css"
            }),
            new OptimizeCssAssetsPlugin(),
            new HtmlWebpackPlugin({
                filename: "index.html",
                template: "src/index.html",
                minify: minifyHtml,
                excludeChunks: ["auth"]
            }),
            new HtmlWebpackPlugin({
                filename: "login.html",
                template: "src/login.html",
                minify: minifyHtml,
                chunks: ["auth"]
            })
        ],
        output: {
            filename: "[name].[hash].js"
        }
    }
);
