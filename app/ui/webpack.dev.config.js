const WebpackMerge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const Base = require('./webpack.base.config');

module.exports = WebpackMerge.smart(
    Base,
    {
        mode: "development",
        module: {
            rules: [
                {
                    test: /\.s?css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        "postcss-loader",
                        "sass-loader"
                    ]
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(["public"], {
                root: __dirname.replace("ui", "")
            }),
            new MiniCssExtractPlugin({
                filename: "[name].css",
                chunkFileName: "[id].css"
            })
        ]
    }
);
