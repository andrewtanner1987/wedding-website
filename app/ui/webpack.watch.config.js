require('dotenv').config();
const WebpackMerge = require('webpack-merge');

const Base = require('./webpack.base.config');

module.exports = WebpackMerge.smart(
    Base,
    {
        mode: "development",
        devServer: {
            port: process.env.WATCH_PORT,
            hot: true,
            host: "0.0.0.0",
            watchOptions: {
                poll: true
            }
        }
    }
);
