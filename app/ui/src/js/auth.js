import'./modules/form-error';

export default () => {
    if (module.hot) {
        module.hot.accept([
            './modules/form-error'
        ], () => { console.log('HMR updating...') });
    }
}
