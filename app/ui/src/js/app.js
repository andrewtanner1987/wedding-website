import './modules/hamburger';
import './modules/countdown';
import './modules/overlay';
import './modules/overflow';
import './modules/google-map';
import './modules/accordions';
import './modules/opacity-animate';
import './modules/scroll-fade';

export default () => {
    if (module.hot) {
        module.hot.accept([
            './modules/hamburger',
            './modules/countdown',
            './modules/overlay',
            './modules/overflow',
            './modules/google-map',
            './modules/accordions',
            './modules/opacity-animate',
            './modules/scroll-fade'
        ], () => { console.log('HMR updating...') });
    }
}
