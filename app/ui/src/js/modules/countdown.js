export default (() => {
    const el = document.querySelector('.js-countdown');
    const futureDateInSeconds = Number.parseInt(Date.parse('13 Jun 2019 12:00:00 GMT') / 1000);
    const nowDateInSeconds = Number.parseInt(Date.now() / 1000);

    const hoursToGo = Math.ceil((((
        futureDateInSeconds - nowDateInSeconds) / 60) / 60
    ));

    const daysToGo = Math.floor(hoursToGo / 24) + 1;

    if (daysToGo > 1) {
        el.textContent = `${daysToGo} day${daysToGo > 1 ? 's' : ''} to go!`;
    } else if (daysToGo < 0) {
        el.textContent = 'We\'ve tied the knot!';
    } else {
        el.textContent = `${hoursToGo - 1} hour${hoursToGo - 1 > 1 ? 's' : ''} to go!`;
    }
})();
