import focus from './focus';

export default (() => {
    const overlayControl = document.querySelectorAll('.js-overlay-toggle');

    [...overlayControl].forEach(el => {
        el.addEventListener('click', () => {
            const targetSelector = el.getAttribute('data-overlay-toggle');
            const target = document.querySelector(targetSelector);
            const focussables = target.querySelectorAll('[tabindex="-1"]:not(.js-inner-focus), [tabindex="0"]:not(.js-inner-focus)');

            focus(focussables);
            target.classList.toggle('overlay--is-active');
            focussables[0].focus();
        });
    });
})();
