require('intersection-observer');

import Settings from './settings';

export default (() => {
    if (window.innerWidth >= Settings.breakpoints.large) {
        const elements = document.querySelectorAll('.js-opacity-anim-control');
        
        const handleObservedElements = observedElements => {
            [...observedElements].forEach(observedElement => {
                const animatableElement = observedElement.target.parentNode.querySelector('.js-opacity-anim-element');
                let prevRatio = 0;
                let currentRatio = observedElement.intersectionRatio;

                if (currentRatio != prevRatio) {
                    animatableElement.style.opacity = currentRatio;
                }

                prevRatio = currentRatio;
            });
        };

        const thresholds = () => {
            const thresholds = [];
            const numSteps = 100;
            
            for (let i=0; i<=numSteps; i++) {
                thresholds.push(i/numSteps);
            }
            
            return thresholds;
        }

        window.addEventListener('load', () => {
            const observer = new IntersectionObserver(handleObservedElements, {
                threshold: thresholds()
            });

            [...elements].forEach(el => {
                observer.observe(el);
            });
        });
    }
})();
