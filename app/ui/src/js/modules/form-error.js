import 'url-polyfill';

export default (() => {
    const location = new URL(window.location.href);
    const errorType = location.searchParams.get('form-error');

    if (errorType == 'password') {
        const passwordErrorElements = [...document.querySelectorAll('.js-form-error')];

        passwordErrorElements.forEach(passwordErrorElement => {
            passwordErrorElement.classList.remove('form__message--hidden');
        });
    }
})();
