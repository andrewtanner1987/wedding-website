import Settings from "./settings";

export default (() => {
    const overflowControl = document.querySelectorAll('.js-overflow-toggle');

    [...overflowControl].forEach(el => {
        el.addEventListener('click', () => {
            if (window.innerWidth <= Settings.breakpoints.large) {
                [...document.querySelectorAll('html, body')].forEach(el => {
                    el.classList.toggle('overflow-disabled');
                });
            }
        });
    });
})();
