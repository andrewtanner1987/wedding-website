export default (focussables) => {
    return [...focussables].forEach(el => {
        el.tabIndex == -1 ? el.tabIndex = 0 : el.tabIndex = -1;
    });
}
