const GoogleMaps = require('google-maps');

export default (() => {
    window.addEventListener('load', () => {
        const el = document.getElementById('map');

        GoogleMaps.KEY = 'AIzaSyCZZ47K4kBjENClZl24Nz4fj-MEZoqpn3g';
        GoogleMaps.VERSION = '3.36';
    
        GoogleMaps.load(google => {
            const location = new google.maps.LatLng(53.77638,-1.5059947);
            const map = new google.maps.Map(el, {
                center: location,
                zoom: 14,
                // https://snazzymaps.com/editor/edit-my-style/248133
                styles: require("./google-map-config.json"),
                streetViewControl: false,
                mapTypeControl: false
            });
            new google.maps.Marker({
                position: location,
                map: map
            });
        });
    });
})();
