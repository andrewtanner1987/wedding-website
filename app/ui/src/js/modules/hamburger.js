import Settings from './settings';

export default (() => {
    const hamburger = document.querySelector('.js-hamburger');
    const hamburgerControl = document.querySelectorAll('.js-hamburger-toggle');

    [...hamburgerControl].forEach(el => {
        el.addEventListener('click', () => {
            if (window.innerWidth <= Settings.breakpoints.large) {
                hamburger.classList.toggle('hamburger--is-active');
            }
        });
    });
})();
