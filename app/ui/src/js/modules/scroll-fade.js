require('intersection-observer');

export default (() => {
    window.addEventListener('load', () => {
        const observables = [...document.querySelectorAll('.js-scroll-fade')];

        const observer = new IntersectionObserver(observable => {
            if (observable[0].intersectionRatio > 0) {
                observable[0].target.classList.add('is-active');
                observer.unobserve(observable[0].target);
            }
        }, {
            rootMargin: "20px 0px 20px"
        });

        observables.forEach(observable => {
            observer.observe(observable);
        });
    });
})();
