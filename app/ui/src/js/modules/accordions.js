import focus from './focus';

export default (() => {
    const accordionControls = document.querySelectorAll('.js-accordion-toggle');

    [...accordionControls].forEach(accordionControl => {
        accordionControl.addEventListener('click', () => {
            const accordionContext = accordionControl.parentNode;
            const accordionContent = accordionContext.querySelector('.js-accordion-content');
            const accordionContentHeight = accordionContent.clientHeight;
            const accordionContentContainer = accordionContext.querySelector('.js-accordion-content-container');
            const focussables = accordionContent.querySelectorAll('[tabindex]');

            focus(focussables);
            accordionContext.classList.toggle('accordion--is-active');

            if (!accordionContext.classList.contains('accordion--is-active')) {
                accordionContentContainer.style.height = 0;
            } else {
                accordionContentContainer.style.height = `${accordionContentHeight}px`;
            }
        });
    });
})();
