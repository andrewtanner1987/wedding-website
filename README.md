# Wedding website
A small static project built with [webpack](https://webpack.js.org/).

## Dependencies
* [Node.js](https://nodejs.org/) >= 8.0
* [Yarn](https://yarnpkg.com/) >= 1.12.3
* [Docker](https://www.docker.com/) (optional). If using Docker Toolbox for Windows, please start Docker using 'Run as Administrator'. This ensures that all symlinks can be set correctly when installing Node packages.

## Optional Docker usage
Running `docker-compose up` in `/` will get you going. This fires up a simple Docker container using a Node 8 image. Docker is only used as a development environment, not production.

There is some basic config in `.example.env` to ensure ports for a simple Express server as well as `webpack-dev-server` are exposed to the host machine. Save these changes as a `.env` file in `/`, which is ignored by Git. There is also a `DNS` setting to provide better DNS for Docker to resolve internally.

You can choose to run the app inside Docker with the Docker `exec` command. You need to know the container name which you can find by running `docker ps` once the container is `Attaching...` after running `docker-compose up`. Then type `docker exec -it <container name> <path to bash>`, e.g `docker exec -it weddingwebsite_node_1 bash`.

Docker mounts `/app` at `/var/www`.

## App usage
The app is split into `/server` and `/ui`. Both directories contain their own `package.json` so dependencies are localised.

### Server
A simple Express server makes use of sessions to offer some basic authentication. The password and secret can be set in `/server/.env` using `/server/.example.env` as a template. You should also configure the port Express will run on.

Run `yarn install` in `/server`.

Once all packages are installed you can run:

* `yarn server` to start the Express server on the port specified in `/server/.env`.

### UI
The UI is managed in this directory.

Run `yarn install` in `/ui`.

Once all packages are installed you can run one of the following:

* `yarn dev` to get a development build. Assets are not optimised, minified or hashed.

* `yarn watch` gives the same output as `yarn dev` but starts a `webpack-dev-server` exposed on the port set in `/ui/.env`. Duplicate `/ui/.example.env` to create this file. If using Docker make sure this same port is also exposed in `/.env`.

* `yarn build` creates an optimised build in `/public` including hashed assets for cache-busting.

If you are using `yarn dev` or `yarn build` you can elect to use the Express server to visit in a browser. See notes above on 'Server'.

## Browser compatibility
Efforts have been made to ensure browser compatibility including latest Chrome (Android, Mac OS and Windows), Firefox (Mac OS and Windows), Safari (Mac OS and iOS), Edge and Internet Explorer 11. Internet Explorer 11 is patched to ES6 standards using a combination of Polyfills, Babel, Modernizr and Autoprefixer with PostCSS.